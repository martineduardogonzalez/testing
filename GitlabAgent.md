# Adding a Gitlab Agent to this project

1) In your repository, add the Agent configuration file under:
   ```yml
   .gitlab/agents/<agent name>/config.yaml
   ```
2) Add the following code to config.yaml:
```yml
    ci_access:
    projects:
        - id: "martineduardogonzalez/testing"
        paths:
        - glob: '/**/*.{yaml,yml,json}'
```

3) Go to  Gitlab --> Infrastructure --> Kubernetes Cluster
   - Connect Cluster
   - Add an Agent name ( must be the same name of your directory .gitlab/agents/<agent name>)
   - Register the agent. A modal will show up with the following instructions:

```yml
Connect a Kubernetes cluster
Agent access token:

AxV6VhrwyuLNGZcXKnsKioWDsyk79YWy5VbFJ-eD1sKTb7x-yg

The agent uses the token to connect with GitLab.

You cannot see this token again after you close this window.
Install using Helm (recommended)

From a terminal, connect to your cluster and run this command. The token is included in the command.

helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install testagent gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set image.tag=v15.4.0 \
    --set config.token=AxV6VhrwyuLNGZcXKnsKioWDsyk79YWy5VbFJ-eD1sKTb7x-yg \
    --set config.kasAddress=wss://kas.gitlab.com

Advanced installation methods

View the documentation for advanced installation. Ensure you have your access token available.
```
4) Run the command in the terminal, you should see this output:

```yml
$ helm upgrade --install testagent gitlab/gitlab-agent \
>     --namespace gitlab-agent \
>     --create-namespace \
>     --set image.tag=v15.4.0 \
>     --set config.token=xfjFaofCwhnUGdSpPdknFLQYMvxTtXehC4EKgs7LgzbuT8cLfQ \
>     --set config.kasAddress=wss://kas.gitlab.com
Release "testagent" does not exist. Installing it now.
NAME: testagent
LAST DEPLOYED: Sun Aug 28 18:33:20 2022
NAMESPACE: gitlab-agent
STATUS: deployed
REVISION: 1
TEST SUITE: None
```
* Wait 2 minutes to the Agent goes up
