Having Tested Monitoring on EKS Clusters, next we re gonna apply on Local environment:

K3D Approach

Installation:


curl -s https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash
check k3d version to use for the image pull during cluster creation.
$ k3d version



k3d version v5.4.4
k3s version v1.23.8-k3s1 (default)




Cluster creation:
$ k3d cluster create test -p "80:80@loadbalancer"


Deployment:
$ kubectl apply -f deployment.yaml
Besides of specifying the number of replicas, this manifest will pull the Megamind Frontend image from Docker Hub


Service creation:
$ kubectl apply -f service.yaml
Defined as ClusterIP, the service will expose the port 3000 to the different pods




Testing the Infrastructure of the Cluster:


Making use of port-forwarding, we can check the functionality of the Frontend:


First, run this command to know the pod name to check:
$ kubectl get pods -A


Then, port-forward the node:
$ kubectl port-forward <pod-name> 3000
You will able to see the running Frontend at http://localhost:3000




Provisionating Monitoring : Prometheus & Grafana


Provisionate Prometheus Operator Helm Chart through the following commands:
$ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
$ helm repo update
$ helm install megamindmonitoring prometheus-community/kube-prometheus-stack


Check functionality

Prometheus:
$ kubectl port-forward svc/prometheus-operated 9090


Http://localhost:9090

Grafana
$ kubectl port-forward svc/megamindmonitoring-grafana 3000:80


Http://localhost:3000




Provisionating Observability : DataDog


Provisionate DataDog Helm Chart through the following commands:
$ helm repo add datadog https://helm.datadoghq.com
$ helm repo update

Create an empty datadog-values.yaml

helm install megamind-datadog -f datadog-values.yaml --set datadog.site='datadoghq.com' --set datadog.apiKey= datadog/datadog

Upgrade datadog-values.yaml with the following content:


datadog:
   logs:
     enabled: true
     containerCollectAll: true


$ helm upgrade -f datadog-values.yaml megamind-datadog datadog/datadog

# targetSystem -- Target OS for this deployment
# (possible values: linux, windows)
targetSystem: "linux"
datadog:
# datadog.apiKey -- Your Datadog API key
apiKey: "02e74911a9380e45f213cec2c766f96d"


$ helm upgrade megamind-datadog --set datadog.apiKey=02e74911a9380e45f213cec2c766f96d datadog/datadog
$ helm upgrade -f datadog-values.yaml megamind-datadog datadog/datadog
If the last command fails WITH A CRD ERROR, then run this command:
$ helm upgrade -f datadog-values.yaml megamind-datadog datadog/datadog --skip-crds

Test Observability at:

https://www.datadoghq.com/


## Dont forget to install the runner!




References
https://github.com/jala-bootcamp/LDO02-Megamind-eks-cluster/tree/feature/Helm
https://www.loginradius.com/blog/engineering/rest-api-kubernetes/


